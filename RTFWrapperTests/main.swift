//
//  main.swift
//  RTFWrapperTests
//
//  Created by Steve Clarke on 30/01/2024.
//  Copyright © 2024 Steve Clarke. All rights reserved.
//

import Foundation
import RTFDocWrapper
import RTFDocWrapperOC

let templatePath = "\(FileManager.default.homeDirectoryForCurrentUser.path)/Documents/utilities/cottages_utils/electricity/cottage_template.rtf"
let  (attrString , _, _) = RTFDocumentWrapper.richTextUsing(templatePath)
print(attrString?.string as Any)
assert(attrString?.string.contains(/Westfield/) != nil, "Failed to find Westfield")



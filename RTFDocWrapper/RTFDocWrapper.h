//
//  RTFDocWrapper.h
//  RTFDocWrapper
//
//  Created by Steve Clarke on 04/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for RTFDocWrapper.
FOUNDATION_EXPORT double RTFDocWrapperVersionNumber;

//! Project version string for RTFDocWrapper.
FOUNDATION_EXPORT const unsigned char RTFDocWrapperVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RTFDocWrapper/PublicHeader.h>



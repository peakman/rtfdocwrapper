//
//  rtf_wrapper.swift
//  RTFDocWrapper
//
//  Created by Steve Clarke on 04/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation
import AppKit
import RTFDocWrapperOC

open class RTFDocumentWrapper : RTFDocumentWrapperOC {
    public let attributedString : NSMutableAttributedString
    open var docAttrs : [String: AnyObject]
    open var fileWrapper : FileWrapper {
        var wrapper : FileWrapper? = nil
        let result = self.makeFileWrapper(attributedString, docAttrs: docAttrs )!
        if let rwk : AnyObject = result.value(forKey: SCResultWrapperKey) as AnyObject? {
            wrapper = (rwk as! FileWrapper)
        } else {
            print("Failed to create file wrapper.  Error is \(result[SCResultErrorKey] as! NSError)", terminator: "")
            assert(false, "Failed to create file wrapper")
            wrapper = nil
        }
        return wrapper!
    }
    
    public init(attrString: NSAttributedString, docAttrs docAttrsIn: [String :  AnyObject] ) {
        attributedString = attrString.mutableCopy() as! NSMutableAttributedString
        docAttrs = docAttrsIn
    }
    
    public init(templatePath: String ) {
        let  (attrString , attributes, _) = RTFDocumentWrapper.richTextUsing(templatePath)
        attributedString = attrString?.mutableCopy() as! NSMutableAttributedString
        docAttrs = attributes!
    }
    
    open class func richTextUsing(_ path : String) -> ( NSAttributedString?,  [String : AnyObject]?, NSError? )   {
        let richTextResult = self.richText(from: path )!
        let attrString = richTextResult[SCResultAttrStringKey] as! NSAttributedString?
        let attrs  = richTextResult[SCResultDocAttrsKey] as! [String : AnyObject]?
        //print("attrs: " , attrs)
        //print("attrString:" ,attrString)
        let error  = richTextResult[SCResultErrorKey] as! NSError?
        var attributes = [ String : AnyObject]()
        if attrs != nil  {
            for assoc in (attrs! ) {
                attributes[assoc.key] = assoc.value as AnyObject
            }
        }
        return (attrString , attributes, error )
    }
    
    open func setDocAttribute( _ key : String , value : AnyObject!) {
        docAttrs[key] = value
    }
    
    open func appendAttributedString( _ attrString : NSAttributedString) {
        attributedString.append(attrString)
    }
    
    open func appendString( _ string : String) {
        attributedString.append(NSAttributedString(string: string))
    }
    
    open func writeToDir( _ outDir : String , name: String) -> NSError! {
        let res : NSDictionary = self.write(fileWrapper,
                                            to: URL(fileURLWithPath: outDir).appendingPathComponent(name))
        return res[SCResultErrorKey] as? NSError
    }

}



# README #

This repository provides a framework to facilitate reading and writing RTF documents from Swift.  

The class  RTFDocumentWrapperOC (see separate repository) wraps an RTF document and provides methods that I couldn't work out how to code in Swift. 

This Swift framework, RTFDocWrapper, includes the class RTFDocumentWrapper that provides the Swift api for RTF processing.  I intend to add functions to make it easy to write arrays as tables.  It won't exactly be prawn (for those familiar with this great pdf gem for ruby) but it will provide some basic features.

I couldn't get the tests to work because the Objective-C superclass wouldn't load.  It works OK in a normal executable but not within the testing context.